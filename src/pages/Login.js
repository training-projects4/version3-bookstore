import { useState, useEffect,useContext} from 'react';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import styled, { css } from 'styled-components'



export default function Login(){
    const navigate = useNavigate();
	const { user, setUser } = useContext(UserContext);
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ isActive, setIsActive ] = useState(true);

    useEffect(() => {
		if(email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])

	function authentication(e) {
		e.preventDefault();

	fetch('https://dandbookstore.herokuapp.com/api/auth/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken);
				setUser({
					accessToken: data.accessToken
				})
				Swal.fire({
					title: 'Yaay!',
					icon: 'success',
					text: 'Welcome to D & D Bookstore!'
				})
                

				fetch('https://dandbookstore.herokuapp.com/api/users/profile/', {
					headers:{
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					if(data.isAdmin === true){
						localStorage.setItem('isAdmin', data.isAdmin)
						setUser({
							isAdmin: data.isAdmin
						})
					
						navigate ('/admin')
					}else{
					
						navigate('/')
					}
				})
			}
            else{
				Swal.fire({
					title: 'Ooops!',
					icon: 'error',
					text: 'Incorrect username or password.'
				})
				
			}
			setEmail('');
			setPassword('');

		})

	}

    return(
        (user.accessToken !== null) ?
		<Navigate to="/" />
		:
        <div className='main-wrapper'>
            <LoginStyle>
            <div className='login-wrapper'>
                <div>
                    <h2 className='title'>Login</h2>
                </div>
                <form className='form-wrapper' onSubmit={e => authentication (e)}>
                    <div className='email'>
                        <label className='label'>Email</label>
                        <input className='input' type="email" name="email" value={email} onChange={e=>setEmail(e.target.value)}required />
                    </div>
                    <div className='password'>
                        <label className='label'>Password </label>
                        <input className='input' type="password" name="password" value={password} onChange={e=>setPassword(e.target.value)} required />
                    </div>
                    <div>
                        {isActive ?
                        <button className='submit'>Login</button>
                        :
                        <button className='submit' disabled>Login</button>
                        }
                    </div>
                </form>

            </div>
            </LoginStyle>
        </div>
    )
}

const LoginStyle = styled.div`
.login-wrapper{
     background-color:var(--white);
    min-width: 350px;
    min-height: 500px;
    padding:50px;
    box-sizing: border-box;
    border-radius: 5px;
}

`;
